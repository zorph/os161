
    )   (                      
 ( /(   )\ )    )  (        )  
 )\()) (()/( ( /(  )\ )  ( /(  
((_)\   /(_)))\())(()/(  )\()) 
  ((_) (_)) ((_)\  /(_))((_)\  
 / _ \ / __| / (_)(_) /  / (_) 
| (_) |\__ \ | |   / _ \ | |   
 \___/ |___/ |_|   \___/ |_|   
    DESIGN DOCUMENT FOR A4

OVERVIEW
--------

We choose to implement A4 on top of A3 to have a reliable thread system to enable testing of the A4 solution.  Although the A2 solution was desirable, after two weeks of work we were forced to rebuild our project from A3 when bugs developed in the thread system that were present in the initial code base.  The thread system for testing was seen as invaluable to the project.  The thread system, however, remains untouched, as the only files that were made were to the functions that needed implementing.  Below is a list of these changes.


TESTING STRATEGY
----------------

-We began with working thread and SFS systems(passing fs1-5).  Incremental changes and careful attention to test results ensured that problems were caught quickly.  The functionality of the thread and SFS filesystem were maintained throughout development.
-We enhanced filetest and bigfile to run with no arguments to test our filetable and file operations.
-We created the user test filefork which preforms writes 'hello world' to a file opened by the parent and copied to the child.


FILETABLE DESIGN
________________

-Our open filetable design supports fork semantics trough carefull use of attribute types.
-The overall design strategy for the filetable involved the use of pointers for instances that we wanted to be global to a process group, and the use of instance variables for attributes of the filetable that we wanted to be specific to each filetable.  The proper use of filetable specific variables also means that they will copy once to the parents initial value, and then maintain their on unique value.

-FD's are implicit as the position in an array of filetable structs.

-We treat the console as we treat any other file.

-Duplicate counts on the vnode are kept as a pointer in the parents filetable struct.  This way it becomes global to a process group, and can keep an accurate count for our close function.

-The actual vnode is kept as a pointer in the parents filetable entry as well, so that it is global to process groups.  FD's that are copied simply share pointers to the vnode (as well as other FD attributes).

-Struct filetable is zeroed on creation off a thread, so it does not need to be freed on exit.

-The files offset is not a pointer so it will not be shared after it is copied, although it will initially have the value of the parents.

SYNCHRONIZATION
---------------

-A lock, used to control access to the filetable's duplicate count and vnode read/write operations, is initialized as a pointer in the parents thread as well, to become global to a process group.


SYSCALLS
--------

FILE OPEN
---------
Most of the work in file open is preformed by vfs functions, but we are careful to check for proper flags and arguments, and to check for the O_APPEND condition.  Normally, a new files offset is 0, but with O_APPEND it is initially the end of the file, and VOP_STAT finds this information for us.  We are careful not to pass the O_APPEND (or any additional unwanted flags) to vfs_open. 

FILE CLOSE
----------
When we close a file, we first check the duplicate count.  If the file has been duplicated within a process group, we simple decrement its duplicate count.  Otherwise, we cal vfs_close to close the file, and free the memory it was using.

READ/WRITE
-----------
VOP functions do most of the work here.  Because we treat console files like regular files, no additional work is required.  The offset of the file is updated following each operation.

DUP2
----
Treats the console as any other file.  Checks if newfd is open, and closes it.

CHANGE DIRECTORY
----------------
sys_chdir function retrieves the user pointer object, and stores it in path. Then, the function makes sure that the path is not too long and then calls vfs_lookup in order to
find the vnode of the desired directory. If successful, the function makes it the current
working directory by calling vfs_setcurdir.

SET CURRENT DIRECTORY
----------------------
sys___getcwd ensures that the passed length is not zero, and then makes a useruio and calls
the vfs_getcwd which in turn writes the current directory's name to the buffer pointer.
Afterwards, the retval value is set to the number of bytes read in the directory name.

FSTAT
-----
sys_fstat locates the desired file with appopriate error checking (makes sure it's in the
file table). It then calls VOP_STAT and makes a useruio. Afterwards, the function uses the
uiomethod to make the user pointer statptr to point to the found stat struct.

GET DIRECTORY ENTRY
-------------------
sys_getdirentry find the file in the filetable with the appopriate error checking. Similarly to getcwd, it then makes a useruio and calls VOP_GETDIRENTRY to get the user pointer to point
to the name of the directory entry. Then, retval is set to length of the directory entry that
was read, and then the offsets are updated.



SFS_GETDIRENTRY
---------------
This is called from the getdirentry syscall. Just like all other sfs methods, a big lock is acquired to assure synchronisation, and is released upon errors or end of function. The idea of this function is given a vnode and a uio, we get the number of directories in the vnode's data, then we read each directory from its slot and update the offset of the uio before using uiomove to transfer data between buffers. 


Biggest Challenge: using space in SFS inodes
--------------------------------------------
Attempting using the wasted space in inodes proved to be our biggest challenge; in fact, it was so great that we never succeeeded in doing it. We understood how the process would work
in theory: data would be stored in the sfs inode with metadata about the file, and that uiomove would be used to move data from the inode and to the output. Nevertheless, we were unsure where exactly in sfs_io, sfs_partial_io and sfs_truncate should this be implemented
and how exactly will the offsets be handled.